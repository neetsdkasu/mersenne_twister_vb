' Mersenne Twister 移植
' Leonardone @ NEETSDKASU
' BSD-2-Clause License

Public Class MersenneTwister
    Private Const N As Integer = 624
    Private Const M As Integer = 397
    Private Const MatrixA As ULong = &H9908b0dful
    Private Const UpperMask As ULong = &H80000000ul
    Private Const LowerMask As ULong = &H7ffffffful
    Private Const DefaultSeed As ULong = 5489ul

    Private Shared ReadOnly Mag01 As ULong() = {0ul, MersenneTwister.MatrixA}

    Private mt(MersenneTwister.N - 1) As ULong
    Private mti As Integer

    Public Sub New()
        MyClass.New(MersenneTwister.DefaultSeed)
    End Sub

    Public Sub New(ByVal seed As UInteger)
        MyClass.SetSeed(seed)
    End Sub

    Public Sub New(ByVal keyArray As UInteger())
        MyClass.SetSeed(keyArray)
    End Sub

    ' /* initializes mt[N] with a seed */
    Public Sub SetSeed(ByVal seed As UInteger)
        Me.mt(0) = CULng(seed)
        For i As Integer = 1 To MersenneTwister.N - 1
            Me.mt(i) = 1812433253ul * (Me.mt(i - 1) Xor (Me.mt(i - 1) >> 30)) + CULng(i)
            Me.mt(i) = Me.mt(i) And &Hfffffffful
        Next i
        Me.mti = MersenneTwister.N
    End Sub

    ' /* initialize by an array */
    Public Sub SetSeed(ByVal keyArray As UInteger())
        MyClass.SetSeed(19650218ul)
        Dim i As Integer = 1
        Dim j As Integer = 0
        For k As Integer = 1 To Math.Max(keyArray.Length, MersenneTwister.N)
            Me.mt(i) = (Me.mt(i) Xor ((Me.mt(i - 1) Xor (Me.mt(i - 1) >> 30)) * 1664525ul)) + CULng(keyArray(j)) + CULng(j)
            Me.mt(i) = Me.mt(i) And &Hfffffffful
            i += 1
            If i >= MersenneTwister.N Then
                Me.mt(0) = Me.mt(MersenneTwister.N - 1)
                i = 1
            End If
            j += 1
            If j >= keyArray.Length Then
                j = 0
            End If
        Next k
        For k As Integer = 1 To MersenneTwister.N - 1
            Me.mt(i) = (Me.mt(i) Xor ((Me.mt(i - 1) Xor (Me.mt(i - 1) >> 30)) * 1566083941ul)) - CULng(i)
            Me.mt(i) = Me.mt(i) And &Hfffffffful
            i += 1
            If i >= MersenneTwister.N Then
                Me.mt(0) = Me.mt(MersenneTwister.N - 1)
                i = 1
            End If
        Next k
        Me.mt(0) = &H80000000ul
    End Sub

    ' /* generates a random number on [0,0xffffffff]-interval */
    Public Function GenrandUint32() As UInteger
        Dim y As ULong

        ' /* generate N words at one time */
        If Me.mti >= MersenneTwister.N Then
            Dim kk As Integer
            For kk = 0 To MersenneTwister.N - MersenneTwister.M - 1
                y = (Me.mt(kk) And MersenneTwister.UpperMask) Or (Me.mt(kk + 1) And MersenneTwister.LowerMask)
                Me.mt(kk) = Me.mt(kk + MersenneTwister.M) Xor (y >> 1) Xor MersenneTwister.Mag01(CInt(y And &H1ul))
            Next kk
            For kk = kk To MersenneTwister.N - 2
                y = (Me.mt(kk) And MersenneTwister.UpperMask) Or (Me.mt(kk + 1) And MersenneTwister.LowerMask)
                Me.mt(kk) = Me.mt(kk + (MersenneTwister.M - MersenneTwister.N)) Xor (y >> 1) Xor MersenneTwister.Mag01(CInt(y And &H1ul))
            Next kk
            y = (Me.mt(MersenneTwister.N - 1) And MersenneTwister.UpperMask) Or (Me.mt(0) And MersenneTwister.LowerMask)
            Me.mt(MersenneTwister.N - 1) = Me.mt(MersenneTwister.M - 1) Xor (y >> 1) Xor MersenneTwister.Mag01(CInt(y And &H1ul))
            Me.mti = 0
        End If

        y = Me.mt(Me.mti)
        Me.mti += 1

        ' /* Tempering */
        y = y Xor (y >> 11)
        y = y Xor ((y << 7) And &H9d2c5680ul)
        y = y Xor ((y << 15) And &Hefc60000ul)
        y = y Xor (y >> 18)

        Return CUInt(y)

    End Function

    ' /* generates a random number on [0,0x7fffffff]-interval */
    Public Function GenrandUint31() As UInteger
        Return Me.GenrandUint32() >> 1
    End Function

    ' /* generates a random number on [0,1]-real-interval */
    Public Function GenrandReal1() As Double
        Return CDbl(Me.GenrandUint32()) * (1.0 / 4294967295.0)
        ' /* divided by 2^32-1 */
    End Function

    ' /* generates a random number on [0,1)-real-interval */
    Public Function GenrandReal2() As Double
        Return CDbl(Me.GenrandUint32()) * (1.0 / 4294967296.0)
        ' /* divided by 2^32 */
    End Function

    ' /* generates a random number on (0,1)-real-interval */
    Public Function GenrandReal3() As Double
        Return (CDbl(Me.GenrandUint32()) + 0.5) * (1.0 / 4294967296.0)
        ' /* divided by 2^32 */
    End Function

    ' /* generates a random number on [0,1) with 53-bit resolution*/
    Public Function GenrandRes53() As Double
        Dim a As UInteger = Me.GenrandUint32() >> 5
        Dim b As UInteger = Me.GenrandUint32() >> 6
        Return (CDbl(a) * 67108864.0 + CDbl(b)) * (1.0 / 9007199254740992.0)
    End Function

End Class

'/*
' *  疑似乱数生成機(RNG)  移植(Porting)
' *  Information of Original Source
' *  Mersenne Twister with improved initialization (2002)
' *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/mt.html
' *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/mt19937ar.html
' */
'// = 移植元ラインセンス (License of Original Source) =======================================================
'// ======================================================================
'/*
'   A C-program for MT19937, with initialization improved 2002/1/26.
'   Coded by Takuji Nishimura and Makoto Matsumoto.
'
'   Before using, initialize the state by using init_genrand(seed)
'   or init_by_array(init_key, key_length).
'
'   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
'   All rights reserved.
'
'   Redistribution and use in source and binary forms, with or without
'   modification, are permitted provided that the following conditions
'   are met:
'
'     1. Redistributions of source code must retain the above copyright
'        notice, this list of conditions and the following disclaimer.
'
'     2. Redistributions in binary form must reproduce the above copyright
'        notice, this list of conditions and the following disclaimer in the
'        documentation and/or other materials provided with the distribution.
'
'     3. The names of its contributors may not be used to endorse or promote
'        products derived from this software without specific prior written
'        permission.
'
'   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
'   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
'   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
'   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
'   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
'   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
'   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
'   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
'   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
'   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
'   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'
'
'   Any feedback is very welcome.
'   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
'   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
'*/
'// ======================================================================
'